<?php

/**
 * La partie Java n'enregistait pas correctemnt l'authority_id de la table journal
 * (il s'agit d'une dénormalisation de cette table pour des soucis de performances)
 *
 * Cela crée un bug lors de l'affichage des journaux des collectivités (la requête utilisant les colonnes dénormalisé)
 * Ce script est à passé avant le passage à la version 2.4 de S2LOW
 *
 */

use S2lowLegacy\Lib\SQLQuery;

require_once(__DIR__ . "/../../init/init.php");
$sqlQuery = \S2lowLegacy\Class\LegacyObjectsManager::getLegacyObjectInstancier()->get(SQLQuery::class);

echo "Correction authority_id\n";
$sql = "UPDATE logs SET authority_id=users.authority_id FROM users " .
        " WHERE logs.user_id=users.id AND logs.authority_id IS NULL AND logs.user_id IS NOT NULL";

$sqlQuery->query($sql);

echo "Correction authority_group_id\n";
$sql = "UPDATE logs SET authority_group_id=authorities.authority_group_id FROM authorities " .
    " WHERE logs.authority_id=authorities.id AND logs.authority_group_id IS NULL AND logs.authority_id IS NOT NULL";
$sqlQuery->query($sql);

echo "ok\n";
