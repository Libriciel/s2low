//import $ from 'jquery';
//import jQuery from 'jquery';

// log removed - adds an extra dependency
//log(jQuery.browser)

require ('jquery-ui');
/*require('jquery-ui/ui/widgets/autocomplete');
require('jquery-ui/themes/base/autocomplete.css');
require('jquery-ui/ui/widgets/menu');
require('jquery-ui/ui/widgets/selectmenu');
require('jquery-ui/themes/base/core.css');
require('jquery-ui/themes/base/theme.css');
require('jquery-ui/themes/base/selectable.css');
require('jquery-ui/themes/base/menu.css');
require('jquery-ui/themes/base/selectmenu.css');
require('jquery-ui/ui/core');
require('jquery-ui/ui/widgets/selectable');*/

require('jquery-ui/ui/core.js');
require('jquery-ui/themes/base/core.css');

require('jquery-ui/ui/widget.js');


require('jquery-ui/ui/widgets/mouse.js');
require('jquery-ui/ui/position.js');

require('jquery-ui/ui/widgets/accordion.js');
require('jquery-ui/themes/base/accordion.css');

require('jquery-ui/ui/widgets/autocomplete.js');
require('jquery-ui/themes/base/autocomplete.css');

require('jquery-ui/ui/widgets/button.js');
require('jquery-ui/themes/base/button.css');


require('jquery-ui/ui/widgets/datepicker.js');
require('jquery-ui/themes/base/datepicker.css');
require('jquery-ui/ui/i18n/datepicker-fr');
$.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );

require('jquery-timepicker/jquery.timepicker');
require('jquery-timepicker/jquery.timepicker.css');

require('jquery-ui/ui/widgets/dialog.js');
require('jquery-ui/themes/base/dialog.css');

require('jquery-ui/ui/widgets/draggable.js');
require('jquery-ui/themes/base/draggable.css');

require('jquery-ui/ui/widgets/droppable.js');

require('jquery-ui/ui/widgets/menu.js');
require('jquery-ui/themes/base/menu.css');

require('jquery-ui/ui/widgets/progressbar.js');
require('jquery-ui/themes/base/progressbar.css');

require('jquery-ui/ui/widgets/resizable.js');
require('jquery-ui/themes/base/resizable.css');

require('jquery-ui/ui/widgets/selectable.js');
require('jquery-ui/themes/base/selectable.css');

require('jquery-ui/ui/widgets/selectmenu.js');
require('jquery-ui/themes/base/selectmenu.css');

require('jquery-ui/ui/widgets/slider.js');
require('jquery-ui/themes/base/slider.css');

require('jquery-ui/ui/widgets/sortable.js');
require('jquery-ui/themes/base/sortable.css');

require('jquery-ui/ui/widgets/spinner.js');
require('jquery-ui/themes/base/spinner.css');

require('jquery-ui/ui/widgets/tabs.js');
require('jquery-ui/themes/base/tabs.css');

require('jquery-ui/ui/widgets/tooltip.js');
require('jquery-ui/themes/base/tooltip.css');


require('jquery-ui/themes/base/theme.css');

require('jquery-ui/ui/effect.js');
require('jquery-ui/ui/effects/effect-blind.js');
require('jquery-ui/ui/effects/effect-bounce.js');
require('jquery-ui/ui/effects/effect-clip.js');
require('jquery-ui/ui/effects/effect-drop.js');
require('jquery-ui/ui/effects/effect-explode.js');
require('jquery-ui/ui/effects/effect-fade.js');
require('jquery-ui/ui/effects/effect-fold.js');
require('jquery-ui/ui/effects/effect-highlight.js');
require('jquery-ui/ui/effects/effect-puff.js');
require('jquery-ui/ui/effects/effect-pulsate.js');
require('jquery-ui/ui/effects/effect-scale.js');
require('jquery-ui/ui/effects/effect-shake.js');
require('jquery-ui/ui/effects/effect-size.js');
require('jquery-ui/ui/effects/effect-slide.js');
require('jquery-ui/ui/effects/effect-transfer.js');

window.$ = $;
//window.jquery = jquery;

