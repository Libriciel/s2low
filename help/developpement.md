# Tests unitaires


```bash
cd SOURCE_S2LOW
```

Il est n�cessaire d'installer les d�pendances require-dev sp�cifi� dans le fichier composer.json.
```bash
composer install
```

Afin de lancer les tests :

```bash
vendor/bin/phpunit -c test/PHPUnit/
```


