LISTEN 8080
LISTEN 8443

<VirtualHost *:8080>
    ServerAdmin root@localhost
    ServerName localhost
    ServerAlias *

    DocumentRoot /var/www/s2low/public

    Header always append X-Frame-Options SAMEORIGIN

    <Directory /var/www/s2low/public>
        Options +FollowSymLinks -Indexes
    </Directory>
    AddDefaultCharset UTF-8

    RewriteEngine on
    RewriteRule ^/?/?modules/mail(.*)$ https://${S2LOW_WEBSITE_MAIL}:${WEB_HTTPS_PORT}/modules/mail$1 [R=301,L]
</VirtualHost>

<VirtualHost *:8443>
    ServerAdmin root@localhost
    ServerName localhost
    ServerAlias ${S2LOW_WEBSITE}
    DocumentRoot /var/www/s2low/public.ssl
    AddDefaultCharset UTF-8

    # HTTP Strict Transport Security (HSTS)
    Header always set Strict-Transport-Security "max-age=16070400; includeSubDomains"
    # Clickjacking
    Header set X-Frame-Options "DENY"
    <FilesMatch "\.(appcache|atom|bbaw|bmp|crx|css|cur|eot|f4[abpv]|flv|geojson|gif|htc|ico|jpe?g|js|json(ld)?|m4[av]|manifest|map|mp4|oex|og[agv]|opus|otf|pdf|png|rdf|rss|safariextz|svgz?|swf|topojson|tt[cf]|txt|vcard|vcf|vtt|webapp|web[mp]|webmanifest|woff2?|xloc|xml|xpi)$">
    Header unset X-Frame-Options
    </FilesMatch>

    SSLUseStapling ${S2LOW_USE_STAPLING}
    SSLStaplingReturnResponderErrors ${S2LOW_STAPLING_RESPONDER_ERRORS}

    #PHP
    php_admin_value max_file_uploads 200
    php_admin_value upload_max_filesize 200M
    php_admin_value post_max_size 200M
    php_admin_flag mail.add_x_header off

    #LOGS
    LogLevel ${APACHE_LOG_LEVEL}
    ErrorLog /data/log/apache2/s2low-error.log
    CustomLog /data/log/apache2/s2low-access.log combined

    #SSL
    SSLEngine on
    SSLVerifyClient require
    SSLVerifyDepth 5
    SSLCertificateFile /etc/apache2/ssl/fullchain.pem
    SSLCertificateKeyFile /etc/apache2/ssl/privkey.pem
    # Configuration SSL authentification utilisateurs
    SSLCACertificatePath  /etc/s2low/ssl/validca
    SSLCARevocationPath /etc/s2low/ssl/validca
    SSLCARevocationCheck chain
    SSLSessionCacheTimeout 1500
    SSLCipherSuite ${CIPHER_SUITE}
    SSLHonorCipherOrder on
    SSLProtocol ${SSL_PROTOCOL}
    SSLCompression off

    SSLInsecureRenegotiation on
    SSLOptions +StdEnvVars +OptRenegotiate +ExportCertData +LegacyDNStringFormat

	<Directory /var/www/s2low/public.ssl>

        RewriteEngine On
        RewriteCond %{REQUEST_URI}::$1 ^(/.+)/(.*)::\2$
        RewriteRule ^(.*) - [E=BASE:%1]

        RewriteCond %{ENV:REDIRECT_STATUS} ^$
        RewriteRule ^index\.php(?:/(.*)|$) %{ENV:BASE}/$1 [R=301,L]

        RewriteRule ^index\.php - [L]

        RewriteCond %{REQUEST_FILENAME} -f
        RewriteCond %{REQUEST_FILENAME} !^.+\.php$
        RewriteRule ^ - [L]

        RewriteRule ^ %{ENV:BASE}/index.php [L]

        Options -Indexes +FollowSymLinks +MultiViews
        AllowOverride All
        Require all granted

        LimitRequestBody 200486000
        SSLRenegBufferSize 200486000
	</Directory>

    Alias /libersign/ /var/www/parapheur/libersign/
	<Directory /var/www/parapheur/libersign/>
        	SSLVerifyClient none
	        Options -Indexes +FollowSymLinks
	        Order   allow,deny
	        Allow   from all
	</Directory>

	ExpiresActive On

    <filesMatch "\.(ico|pdf|flv|jpg|jpeg|png|gif|js|css|swf|css)$">
        ExpiresDefault "access plus 5 minutes"
        Header set Cache-Control "max-age=300, must-revalidate"
    </filesMatch>

</VirtualHost>

<VirtualHost *:8443>
    ServerAdmin root@localhost
    ServerName ${S2LOW_WEBSITE_MAIL}
    DocumentRoot /var/www/s2low/public
    AddDefaultCharset UTF-8

    # HTTP Strict Transport Security (HSTS)
    Header always set Strict-Transport-Security "max-age=16070400; includeSubDomains"

    # Clickjacking
    Header set X-Frame-Options "DENY"
    <FilesMatch "\.(appcache|atom|bbaw|bmp|crx|css|cur|eot|f4[abpv]|flv|geojson|gif|htc|ico|jpe?g|js|json(ld)?|m4[av]|manifest|map|mp4|oex|og[agv]|opus|otf|pdf|png|rdf|rss|safariextz|svgz?|swf|topojson|tt[cf]|txt|vcard|vcf|vtt|webapp|web[mp]|webmanifest|woff2?|xloc|xml|xpi)$">
    Header unset X-Frame-Options
    </FilesMatch>

    SSLUseStapling ${MAIL_USE_STAPLING}
    SSLStaplingReturnResponderErrors ${MAIL_STAPLING_RESPONDER_ERRORS}

    #PHP
    php_admin_value max_file_uploads 200
    php_admin_value upload_max_filesize 200M
    php_admin_value post_max_size 200M
    php_admin_flag mail.add_x_header off

    #LOGS
    LogLevel info
    ErrorLog /data/log/apache2/s2low-mailsec-error.log
    CustomLog /data/log/apache2/s2low-mailsec-access.log combined

    #SSL
    SSLEngine on
    SSLCertificateFile /etc/apache2/ssl/mailsec_fullchain.pem
    SSLCertificateKeyFile /etc/apache2/ssl/mailsec_privkey.pem

    SSLSessionCacheTimeout 1500
    SSLCipherSuite  ${CIPHER_SUITE}
    SSLHonorCipherOrder on
    SSLProtocol ${SSL_PROTOCOL}
    SSLCompression off

    SSLInsecureRenegotiation on
    SSLOptions +StdEnvVars +OptRenegotiate +ExportCertData +LegacyDNStringFormat
    <Directory /var/www/s2low/public>
        Options +FollowSymLinks -Indexes
    </Directory>

</VirtualHost>
