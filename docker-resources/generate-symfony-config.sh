#! /bin/bash
set -e

# Utiliser pour creer un fichier de settings symfony en fonction des variables d'environnement (envoye par Docker)
#TODO : integrer les variables WEB_HTTP_PORT et WEB_HTTPS_PORT

cat <<EOF
APP_ENV=${APP_ENV:-test}
APP_SECRET=${APP_SECRET:-26d5b9f65f6caa90a7d55e57550e196c}
APP_DEBUG=${APP_DEBUG:-true}

MAILER_DSN="${MAILER_DSN:-smtp://maildev:1025?verify_peer=0}"

ACTES_APPLI_TRIGRAMME=${ACTES_APPLI_TRIGRAMME:-abc}

HELIOS_FTP_P_APPLI=${HELIOS_FTP_P_APPLI:-GHELPES2}

HELIOS_FTP_SERVER=${HELIOS_FTP_SERVER:-ftp}
HELIOS_FTP_PORT=${HELIOS_FTP_PORT:-21}
HELIOS_FTP_PASSIVE_MODE=${HELIOS_FTP_PASSIVE_MODE:-true}
HELIOS_FTP_CONNECTION_MODE=${HELIOS_FTP_CONNECTION_MODE:-false}
HELIOS_FTP_LOGIN=${HELIOS_FTP_LOGIN:-helios}
HELIOS_FTP_PASSWORD=${HELIOS_FTP_PASSWORD:-helios}
HELIOS_SENDING_DESTINATION=${HELIOS_SENDING_DESTINATION:-/entree/}
HELIOS_FTP_RESPONSE_SERVER_PATH=${HELIOS_FTP_RESPONSE_SERVER_PATH:-/sortie/}

HELIOS_PASSTRANS_SERVER=${HELIOS_PASSTRANS_SERVER:-ftp}
HELIOS_PASSTRANS_PORT=${HELIOS_PASSTRANS_PORT:-21}
HELIOS_PASSTRANS_PASSIVE_MODE=${HELIOS_PASSTRANS_PASSIVE_MODE:-true}
HELIOS_PASSTRANS_CONNECTION_MODE=${HELIOS_PASSTRANS_CONNECTION_MODE:-false}
HELIOS_PASSTRANS_LOGIN=${HELIOS_PASSTRANS_LOGIN:-helios-passtrans}
HELIOS_PASSTRANS_PASSWORD=${HELIOS_PASSTRANS_PASSWORD:-helios-passtrans}
HELIOS_PASSTRANS_SENDING_DESTINATION=${HELIOS_PASSTRANS_SENDING_DESTINATION:-/entree/}
HELIOS_PASSTRANS_RESPONSE_SERVER_PATH=${HELIOS_PASSTRANS_RESPONSE_SERVER_PATH:-/sortie/}

BEANSTAKLD_SERVER=${BEANSTAKLD_SERVER:-beanstalkd}
BEANSTAKLD_PORT=${BEANSTAKLD_PORT:-11300}

REDIS_SERVER=${REDIS_SERVER:-redis}
REDIS_PORT=${REDIS_PORT:-6379}
EOF