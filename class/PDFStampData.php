<?php

namespace S2lowLegacy\Class;

class PDFStampData
{
    /* Date en ISO */

    public $envoi_prefecture_date;
    public $recu_prefecture_date;
    public $affichage_date;
    public $identifiant_unique;
}
