<?php

namespace S2lowLegacy\Class\actes;

use S2lowLegacy\Class\IWorker;
use Exception;

class ActesEnvoiSaeWorker implements IWorker
{
    public const QUEUE_NAME = 'actes-envoi-sae';
    private const MAX_NUMBER_OF_SIMULTANEOUS_PENDING_ARCHIVE = 100;

    private $actesArchiveControler;

    public function __construct(
        ActesArchiveControler $actesArchiveControler
    ) {
        $this->actesArchiveControler = $actesArchiveControler;
    }

    public function getQueueName()
    {
        return self::QUEUE_NAME;
    }

    public function getData($id)
    {
        return $id;
    }

    public function getAllId()
    {
        return $this->actesArchiveControler->getAllTransactionIdToSend(self::MAX_NUMBER_OF_SIMULTANEOUS_PENDING_ARCHIVE);
    }

    /**
     * @param $data
     * @return void
     * @throws Exception
     */
    public function work($data)
    {
        $this->actesArchiveControler->sendArchive($data);
    }

    public function getMutexName($data)
    {
        return sprintf("actes-transaction-%s", $data);
    }

    public function isDataValid($data)
    {
        return true;
    }

    public function start()
    {
        // TODO: Implement start() method.
    }

    public function end()
    {
        // TODO: Implement end() method.
    }
}
