<?php

namespace S2lowLegacy\Class\actes;

use Exception;
use S2lowLegacy\Lib\SQLQuery;

class TransactionSQL
{
    public const EN_COURS = 999;

    private static $transactionTypes = array (
        "1" => "Transmission d'actes",
        "2" => "Courrier simple",
        "3" => "Demande de pièces complémentaires",
        "4" => "Lettre d'observation",
        "5" => "Déféré au Tribunal Administratif",
        "6" => "Annulation",
        "7" => "Demande de classification"
    );

    private static $natureTransaction = array (
        3 => 'Actes individuels',
        2 => 'Actes réglementaires',
        6 => 'Autres',
        4 => 'Contrats, conventions et avenants',
        1 => 'Délibérations',
        5 => 'Documents budgétaires et financiers'
    );

    private static $status = array (
        -1 => 'Erreur',
        0 => 'Annulé',
        1 => 'Posté',
        2 => 'En attente de transmission',
        3 => 'Transmis',
        4 => 'Acquittement reçu',
        5 => 'Validé',
        6 => 'Refusé',
        7 => 'Document reçu',
        8 => 'Acquittement envoyé',
        9 => 'Document envoyé',
        10 => 'Refus d\'envoi',
        11 => 'Acquittement de document reçu',
        12 => 'Envoyé au SAE',
        13 => 'Archivé par le SAE',
        14 => 'Erreur lors de l\'archivage',
        15 => 'Reçu par le SAE',
        16 => 'Détruite',
        17 => "En attente d'être postée",
        18 => "En attente d'être signée",
        19 => "En attente de transmission au SAE",
        20 => "Erreur lors de l'envoi au SAE",
        21 => "Document reçu (pas d'AR)"
    );

    private static $etat_en_cours = array(1,2,3,4,7,8,17,18);

    /** @var  SQLQuery */
    private $sqlQuery;
    private $filter;
    private $value;
    private $offset;
    private $limit;
    private $order;
    private $sortWay;

    public function __construct(SQLQuery $sqlQuery)
    {
        $this->sqlQuery = $sqlQuery;
        $this->limit = 10;
        $this->offset = 0;
        $this->filter = array();
        $this->value = array();
        $this->order = 'actes_transactions.id';
        $this->sortWay = 'ASC';
    }

    public function getTypes()
    {
        return self::$transactionTypes;
    }

    public function getNatures()
    {
        return self::$natureTransaction;
    }

    public function getStatus()
    {
        return self::$status;
    }

    public function setPageNumber($page_number, $taille_page)
    {
        $this->offset = ($page_number - 1) * $taille_page;
        if ($this->offset < 0) {
            $this->offset = 0;
        }
        $this->limit = $taille_page;
        if (! $this->limit) {
            $this->limit = 10;
        }
    }

    public function setOrder($order, $sortway)
    {
        $this->order = ($order == 'id') ? 'actes_transactions.id' : 'submission_date';
        $this->sortWay = ($sortway == 'asc') ? 'ASC' : 'DESC';
    }

    public function setAuthority($authority_id)
    {
        if (! $authority_id) {
            return ;
        }
        $this->filter[] = "actes_transactions.authority_id=?";
        $this->value[] = $authority_id;
    }

    public function setUserId(array $user_id)
    {
        $this->filter[] = "actes_transactions.user_id IN (" . implode(',', $user_id) . ")";
    }

    public function setNature($nature)
    {
        if (! $nature) {
            return;
        }
        $this->filter[] = "actes_transactions.nature_code = ?";
        $this->value[] = $nature;
    }

    public function setType($type)
    {
        if (! $type || $type == 'all') {
            return;
        }
        $this->filter[] = "actes_transactions.type=?";
        $this->value[] = $type;
    }

    public function setStatus($status)
    {
        if ($status === 'all') {
            return;
        }
        if ($status == self::EN_COURS) {
             $this->filter[] = "actes_transactions.last_status_id  IN (" . implode(',', self::$etat_en_cours) . ")";
        } else {
            $this->filter[] = "actes_transactions.last_status_id  = ?" ;
            $this->value[] = $status;
        }
    }


    public function setNumero($numero)
    {
        if (! $numero) {
            return;
        }
        $this->filter[] = "actes_transactions.number LIKE ?";
        $this->value[] = "%$numero%";
        //$this->value[] = "$numero"; FIX #378
    }

    public function setObjet($objet)
    {
        if (! $objet) {
            return;
        }
        $this->filter[] = "actes_transactions.subject ILIKE ?";
        $this->value[] = "%$objet%";
        //$this->value[] = "$objet"; FIX #378
    }

    public function setDateMinSubmission($date)
    {
        if (! $date) {
            return;
        };
        $this->filter[] = "(SELECT date " .
                        " FROM actes_transactions_workflow atw " .
                        " WHERE actes_transactions.id = atw.transaction_id " .
                        " AND ( atw.status_id = 1 OR atw.status_id = 7 ) LIMIT 1 ) >= ? ";
        $this->value[] = $date;
    }

    public function setDateMaxSubmission($date)
    {
        if (! $date) {
            return;
        }
        $this->filter[] = "(SELECT date " .
                        " FROM actes_transactions_workflow atw " .
                        " WHERE actes_transactions.id = atw.transaction_id " .
                        " AND ( atw.status_id = 1 OR atw.status_id = 7 ) LIMIT 1 ) <= ? ";
        $this->value[] = $date;
    }

    public function setDateMinAck($date)
    {
        if (! $date) {
            return;
        }
        $this->filter[] = "(SELECT date " .
                        " FROM actes_transactions_workflow atw " .
                        " WHERE actes_transactions.id = atw.transaction_id " .
                        " AND atw.status_id =  4 LIMIT 1 ) >= ? ";
        $this->value[] = $date;
    }

    public function setDateMaxAck($date)
    {
        if (! $date) {
            return;
        }
        $this->filter[] = "(SELECT date " .
                        " FROM actes_transactions_workflow atw " .
                        " WHERE actes_transactions.id = atw.transaction_id " .
                        " AND atw.status_id =  4 LIMIT 1 ) <= ? ";
        $this->value[] = $date;
    }

    public function getAll()
    {
        $sql =  "SELECT " .
                " envelope_id,  " .
                " submission_date, "  .
                " users.name," .
                " users.givenname," .
                " authorities.name as authority_name, " .
                " actes_transactions.id as transaction_id, " .
                " type,number,subject,archive_url,nature_descr,actes_transactions.last_status_id " .
                " FROM actes_transactions " .
                " JOIN actes_envelopes ON actes_transactions.envelope_id = actes_envelopes.id " .
                " JOIN users ON actes_envelopes.user_id=users.id " .
                " JOIN authorities ON users.authority_id=authorities.id " .
                " WHERE actes_transactions.id IN ( " .
                    "SELECT id " .
                    " FROM actes_transactions " .
                    $this->getWhere() .
                    " ORDER BY $this->order $this->sortWay " .
                    " LIMIT $this->limit OFFSET $this->offset ) ORDER BY $this->order $this->sortWay";
        $result = $this->sqlQuery->query($sql, $this->value);
        foreach ($result as $i => $line) {
            $result[$i]['type_str'] = self::$transactionTypes[$line['type']];
            $result[$i]['current_status'] = $line['last_status_id'];
            $result[$i]['current_status_name'] = self::$status[$line['last_status_id']];
            $result[$i]['courrier_info'] = $this->getCourrierInfo($line['transaction_id']);
        }
        return $result;
    }

    private function getWhere()
    {
        if (! $this->filter) {
            return "";
        }
        return "WHERE " . implode(" AND ", $this->filter);
    }

    public function getCourrierInfo($transaction_id)
    {
        $result = array();
        $sql = " SELECT at1.id as id, at2.id as related_transaction_id,at1.type as type " .
                " FROM actes_transactions at1 " .
                " LEFT JOIN actes_transactions at2 ON at1.id=at2.related_transaction_id  " .
                " WHERE at1.related_transaction_id=? AND (at2.type != '6' OR at2.type IS NULL) ";

        foreach ($this->sqlQuery->query($sql, $transaction_id) as $line) {
            $rline = array("type" => $line["type"],"type_str" => self::$transactionTypes[$line['type']]);

            if (! $line["related_transaction_id"]) {
                $id =   $line["id"];
                $rline['sens'] = "reçu";
            } else {
                $id =   $line["related_transaction_id"];
                $line['sens'] = "envoye";
            }
            $result[$id] = $rline;
        }
        return $result;
    }

    public function getNbTransaction()
    {
        $where = "";
        if ($this->filter) {
            $where = "WHERE " . implode(" AND ", $this->filter);
        }
        $sql = "SELECT count(id)   " .
                " FROM actes_transactions  " .
                $this->getWhere() ;
        return $this->sqlQuery->queryOne($sql, $this->value);
    }

    public function delete($id)
    {
        $sql = "DELETE FROM actes_included_files WHERE transaction_id=?";
        $this->sqlQuery->query($sql, $id);
        $sql  = "DELETE FROM actes_transactions_workflow WHERE transaction_id=?";
        $this->sqlQuery->query($sql, $id);
        $sql = "DELETE FROM actes_transactions WHERE id=?";
        $this->sqlQuery->query($sql, $id);
    }

    public function getDonneesTransaction($id)
    {
        //$sql = "SELECT decision_date, document_papier, unique_id, classification, classification_string,broadcasted,broadcast_emails FROM actes_transactions WHERE id = ?";
        $sql = "SELECT " .
            " users.name, users.givenname," .
            " authorities.name as authority_name, " .
            " type, number, subject,archive_url,nature_descr, nature_code,decision_date, document_papier, unique_id, classification, classification_string,broadcasted,broadcast_emails" .
            " FROM actes_transactions " .
            " JOIN actes_envelopes ON actes_transactions.envelope_id = actes_envelopes.id " .
            " JOIN users ON actes_envelopes.user_id=users.id " .
            " JOIN authorities ON users.authority_id=authorities.id " .
            " WHERE actes_transactions.id = ?";
        try {
            $result = $this->sqlQuery->query($sql, $id)[0];
            $result['type_str'] = self::$transactionTypes[$result['type']];
            return $result;
        } catch (Exception $e) {
            var_dump($e->getMessage());
            die();
        }
    }

    /**
     * \brief Méthode de récupération du cycle de vie de cette transaction
     * \return Un tableau contenant le workflow de la transaction
     * @param int $id
     * @return array
     * @throws Exception
     */
    public function fetchWorkflow(int $id)
    {
        $sql = "SELECT id, status_id, date, message FROM actes_transactions_workflow WHERE transaction_id= ? ORDER BY date, id ASC";
        return $this->sqlQuery->query($sql, $id);
    }
}
