[![Minimum PHP Version](http://img.shields.io/badge/php-%208.1-8892BF.svg)](https://php.net/)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![pipeline status](https://gitlab.libriciel.fr/libriciel/pole-plate-formes/s2low/s2low/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-plate-formes/s2low/s2low/commits/master)
[![coverage report](https://gitlab.libriciel.fr/libriciel/pole-plate-formes/s2low/s2low/badges/master/coverage.svg)](https://gitlab.libriciel.fr/libriciel/pole-plate-formes/s2low/s2low/commits/master)

# S2low

