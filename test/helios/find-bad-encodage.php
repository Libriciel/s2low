<?php

use S2lowLegacy\Lib\SQLQuery;

require_once(__DIR__ . "/../../init/init.php");
$sqlQuery = \S2lowLegacy\Class\LegacyObjectsManager::getLegacyObjectInstancier()->get(SQLQuery::class);

if (empty($argv[1])) {
    echo "Usage : {$argv[0]} YYYY-mm-dd\n";
    exit;
}
$date = $argv[1];

$sql = "SELECT id,sha1 FROM helios_transactions WHERE submission_date>? AND submission_date<?";
$transactions_list = $sqlQuery->query($sql, $date . " 00:00", $date . " 23:59:59");

$nb_transaction = count($transactions_list);

echo "Analyse de $nb_transaction fichiers\n";


foreach ($transactions_list as $num_transaction => $transaction_helios) {
    echo "Transaction {$transaction_helios['id']} ($num_transaction/$nb_transaction)\n";
    $pes_aller = HELIOS_FILES_UPLOAD_ROOT . "/{$transaction_helios['sha1']}";
    echo "Analyse du fichier : $pes_aller\n";

    $f = @fopen($pes_aller, "r");

    if (! $f) {
        echo "Impossible de lire le fichier\n";
    }

    echo fgets($f, 90);
}
