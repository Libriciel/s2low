<?php

/** @var MessageAdmin $messageAdmin */
/** @var FancyDate $fancyDate */

use S2lowLegacy\Lib\FancyDate;
use S2lowLegacy\Model\MessageAdmin;

?>
<h1 id="message_desc">Message d'urgence</h1>

<p id="back-transaction-btn">
    <a class="btn btn-default" href="/admin/message/">Revenir à la liste des messages
    </a><br>
</p>

<h2>Message : <?php hecho($messageAdmin->titre) ?></h2>




    <table class="data-table table table-striped" aria-describedby="message_desc">
        <tr>
            <th scope="row">Numéro du message</th>
            <td><?php hecho($messageAdmin->message_id) ?></td>
        </tr>
        <tr>
            <th scope="row">État</th>
            <td><?php $messageAdmin->displayEtatLabel() ?></td>
        </tr>
        <tr>
            <th scope="row">Dernier rédacteur</th>
            <td><?php hecho($messageAdmin->user_name) ?></td>
        </tr>
        <tr>
            <th scope="row">Message</th>
            <td>
                <?php $messageAdmin->displayMessage() ?>
            </td>
        </tr>
        <?php if ($messageAdmin->getEtat() != MessageAdmin::ETAT_EN_COURS_DE_REDACTION) : ?>
            <tr>
                <th scope="row">Date de publication</th>
                <td><?php echo $fancyDate->getDateHeureFrancais($messageAdmin->date_publication)?></td>
            </tr>
            <tr>
                <th scope="row">Publieur</th>
                <td><?php hecho($messageAdmin->user_publieur_name)?></td>
            </tr>
            <?php if ($messageAdmin->getEtat() == MessageAdmin::ETAT_RETIRE) : ?>
                <tr>
                    <th scope="row">Date de retrait</th>
                    <td><?php echo $fancyDate->getDateHeureFrancais($messageAdmin->date_retrait)?></td>
                </tr>
                <tr>
                    <th scope="row">Utilisateur ayant retirer le message</th>
                    <td><?php hecho($messageAdmin->user_retireur_name)?></td>
                </tr>
            <?php endif; ?>
        <?php endif; ?>
    </table>

<?php if ($messageAdmin->getEtat() != MessageAdmin::ETAT_RETIRE) : ?>
    <h2>Actions</h2>

    <?php if ($messageAdmin->getEtat() == MessageAdmin::ETAT_EN_COURS_DE_REDACTION) : ?>
        <td>
            <a
                    class='btn btn-warning'
                    href="/admin/message/message_publier.php?message_id=<?php hecho($messageAdmin->message_id) ?>">
                Publier
            </a>
        </td>

        <td>
            <a
                    class='btn btn-primary'
                    href="/admin/message/message_edit.php?message_id=<?php hecho($messageAdmin->message_id) ?>">
                Modifier
            </a>
        </td>
    <?php endif; ?>
    <?php if ($messageAdmin->getEtat() == MessageAdmin::ETAT_PUBLIE) : ?>
        <td>
            <a
                    class='btn btn-warning'
                    href="/admin/message/message_retirer.php?message_id=<?php hecho($messageAdmin->message_id) ?>">
                Retirer
            </a>
        </td>
    <?php endif; ?>
<?php endif; ?>