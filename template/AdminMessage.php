<?php

/** @var MessageAdmin[] $message_list */
/** @var FancyDate $fancyDate */

use S2lowLegacy\Lib\FancyDate;
use S2lowLegacy\Model\MessageAdmin;

?>
<h1>Message d'urgence</h1>

<p id="back-transaction-btn">
    <a class="btn btn-default" href="/admin/index.php">Retour
        </a><br>
</p>

<h2>Actions</h2>
<div id="actions_area">
    <a href="/admin/message/message_edit.php" class="btn btn-primary">Nouveau message</a>
</div>


<h2 id="list_desc">Liste des messages</h2>
<div id="authority-list">
    <table class="data-table table table-striped" aria-describedby="list_desc">
        <thead>
        <tr>
            <th scope="col">Titre</th>
            <th scope="col">Etat</th>
            <th scope="col">Date de publication</th>
            <th scope="col">Date de retrait</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($message_list as $message) :?>
            <tr>
                <td>
                    <a href="/admin/message/detail.php?message_id=<?php hecho($message->message_id) ?>">
                        <?php hecho($message->titre ?: $message->message_id) ?>
                    </a>
                </td>

                <td>
                    <?php $message->displayEtatLabel() ?>
                </td>
                <td>
                    <?php if ($message->is_publie) : ?>
                        <?php echo $fancyDate->getDateHeureFrancais($message->date_publication) ?>
                    <?php endif;?>
                </td>
                <td>
                    <?php if ($message->is_publie) : ?>
                        <?php echo $fancyDate->getDateHeureFrancais($message->date_retrait) ?>
                    <?php endif;?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>