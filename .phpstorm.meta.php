<?php

//https://confluence.jetbrains.com/display/PhpStorm/PhpStorm+Advanced+Metadata

namespace PHPSTORM_META {

    override(\ObjectInstancier::get(0),
        map([
            '' => '@',
        ]));

}
