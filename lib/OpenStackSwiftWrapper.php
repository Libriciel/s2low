<?php

declare(strict_types=1);

namespace S2lowLegacy\Lib;

use S2lowLegacy\Class\CloudStorageException;
use Exception;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Filesystem\Filesystem;
use GuzzleHttp\Psr7\Stream;

/**
 *
 */
class OpenStackSwiftWrapper
{
    private const OPENSTACK_SERVICE = 'swift';

    /** @var OpenStackContainerStore  */
    private OpenStackContainerStore $openStackContainersStore;

    private Filesystem $fileSystem;

    private Logger $logger;
    private bool $openstack_enable;

    /**
     * @param \S2lowLegacy\Lib\OpenStackContainerStore $openStackContainersStore
     * @param \Monolog\Logger $logger
     * @param $openstack_enable
     */
    public function __construct(
        OpenStackContainerStore $openStackContainersStore,
        Logger $logger,
        $openstack_enable
    ) {
        $this->openStackContainersStore = $openStackContainersStore;
        $this->fileSystem = new Filesystem();
        $this->logger = $logger;
        $this->openstack_enable = $openstack_enable;
    }

    /**
     * Envoi un fichier dans les nuages
     * @param string $container_name Le nom du container au sens swift
     * @param string $filepath_local Le chemin local du fichier à envoyer dans les nuages
     * @param string $filename_on_cloud Si présent l'emplacement sur le nuage, sinon, on prend le nom du fichier
     * qu'on met directement sur le container
     * @throws CloudStorageException|UnrecoverableException|PausingQueueException
     */
    public function sendFile(string $container_name, string $filepath_local, string $filename_on_cloud = ''): bool
    {
        if (! $filename_on_cloud) {
            $filename_on_cloud = basename($filepath_local);
        }

        try {
            $fileData = fopen($filepath_local, 'r+');
        } catch (Exception $e) {
            throw new CloudStorageException("Unable to retrieve $filepath_local : " . $e->getMessage());
        }
        if (! $fileData) {
            $error = error_get_last();
            throw new CloudStorageException("Unable to retrieve $filepath_local : $error");
        }
        $stream = new Stream($fileData);
        $this->logger->info("Upload $filepath_local to [$container_name]$filename_on_cloud");
        $fileProperties = [
            'name' => $filename_on_cloud,
            'stream' => $stream
        ];

        $containerWrapper = $this->openStackContainersStore->getContainerWrapper($container_name);

        if ($containerWrapper->createObject($fileProperties)) {
            $this->logger->info("Uploaded $filepath_local to [$container_name]$filename_on_cloud");
            return true;
        }
        return false;
    }

    /**
     * @param string $container_name
     * @param string $filepath_local
     * @param string $filepath_on_cloud
     * @throws \S2lowLegacy\Class\CloudStorageException
     * @throws \S2lowLegacy\Lib\PausingQueueException
     * @throws \S2lowLegacy\Lib\UnrecoverableException
     */

    private function retrieveFileFromCloud(
        string $container_name,
        string $filepath_local,
        string $filepath_on_cloud = ''
    ): void {
        $dirname_local = dirname($filepath_local);

        if (! $this->fileSystem->exists($dirname_local)) {
            $this->fileSystem->mkdir($dirname_local);
        }

        if (! $filepath_on_cloud) {
            $filepath_on_cloud = basename($filepath_local);
        }

        $containerWrapper = $this->openStackContainersStore->getContainerWrapper($container_name);
        if (preg_match('#//+#', $filepath_on_cloud) && ! $containerWrapper->objectExists($filepath_on_cloud)) {
            $filepath_on_cloud = preg_replace('#/+#', '/', $filepath_on_cloud);
            if (!$containerWrapper->objectExists($filepath_on_cloud)) {
                throw new CloudStorageException("$filepath_on_cloud non trouvé dans $container_name");
            }
        }
        $stream = $containerWrapper->download($filepath_on_cloud);

        $this->fileSystem->dumpFile($filepath_local, $stream);

        if (filesize($filepath_local) == 0) {
            unlink($filepath_local);
            throw new CloudStorageException('Erreur lors du téléchargement');
        }
        $this->logger->info("Retrieve [$container_name] $filepath_on_cloud to $filepath_local");
    }

    /**
     * Si nécessaire, récupère et copie le fichier depuis OpenStack vers le système de fichier local
     * @param $container_name : Le nom du container au sens swift
     * @param $filepath_local : Le chemin local du fichier à récupérer
     * @param string $filepath_on_cloud l'emplacement sur le cloud, sinon on prend le nom du fichier local et
     * on le cherche directemnet sur le container
     * @return mixed
     * @throws UnrecoverableException|PausingQueueException|\S2lowLegacy\Class\CloudStorageException
     * @throws \Exception
     */

    public function retrieveFile(
        string $container_name,
        string $filepath_local,
        string $filepath_on_cloud = ''
    ): string {
        if ($this->fileSystem->exists($filepath_local)) {
            return $filepath_local; //
        }
        if (!$this->openstack_enable) {
            throw new Exception("Le fichier $filepath_local n'est pas présent localement, aucun cloud configuré");
        }
        $this->retrieveFileFromCloud($container_name, $filepath_local, $filepath_on_cloud);
        return $filepath_local;
    }

    /**
     * Si nécessaire, récupère et copie le fichier depuis OpenStack vers le système de fichier local
     * @param $container_name : Le nom du container au sens swift
     * @param $filepath_local : Le chemin local du fichier à récupérer
     * @param string $filepath_on_cloud l'emplacement sur le cloud, sinon on prend le nom du fichier local et on le
     * cherche directemnet sur le container
     * @return mixed
     * @throws \Exception
     */

    public function getFileSize(string $container_name, string $filepath_local, string $filepath_on_cloud = ''): array
    {
        if ($this->fileSystem->exists($filepath_local)) {
            return [ filesize($filepath_local), sha1_file($filepath_local) ];
        }
        throw new Exception('[getFileSize]getFileSizeFromCloud non encore implémenté');
        //return  $this->getFileSizeFromCloud($container_name, $filepath_local, $filepath_on_cloud);
    }
    /**
     * @param $container_name
     * @param $filepath
     * @throws UnrecoverableException|PausingQueueException
     */

    public function deleteFile($container_name, $filepath): void
    {
        $filename = basename($filepath);
        $containerWrapper = $this->openStackContainersStore->getContainerWrapper($container_name);
        $containerWrapper->delete($filename);
        $this->logger->info("Delete [$container_name]$filepath");
    }

    /**
     * @param $container_name
     * @param $filename
     * @return bool|ResponseInterface
     */

    public function fileExistsOnCloud($container_name, $filename): bool|ResponseInterface
    {
        try {
            $containerWrapper = $this->openStackContainersStore->getContainerWrapper($container_name);
            return $containerWrapper->objectExists($filename);
        } catch (Exception $e) {
            return false;
        }
    }
}
