<?php

namespace S2lowLegacy\Lib;

class PasswordGenerator
{
    private const NB_SIGNE_DEFAULT = 7;

    private const SIGNE = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private $nbSigne;
    private $signe;
    private $lengthSigne;

    public function __construct()
    {
        $this->nbSigne = self::NB_SIGNE_DEFAULT;
        $this->setSigne(self::SIGNE);
    }

    public function setSigne($signe)
    {
        $this->signe = $signe;
        $this->lengthSigne = mb_strlen($this->signe);
    }

    public function getPassword()
    {
        $password = "";
        for ($i = 0; $i < $this->nbSigne; $i++) {
            $password .= $this->getLettre();
        }
        return $password;
    }

    private function getLettre()
    {
        return $this->signe[rand(0, $this->lengthSigne - 1)];
    }
}
