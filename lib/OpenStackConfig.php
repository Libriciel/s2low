<?php

namespace S2lowLegacy\Lib;

class OpenStackConfig
{
    public $openstack_authentication_url_v3;
    public $openstack_username;
    public $openstack_password;
    public $openstack_tenant;
    public $openstack_region;
    public $openstack_swift_container_prefix;
}
